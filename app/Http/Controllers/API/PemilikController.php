<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Pemilik;
use App\Models\Buku;
use Illuminate\Http\Request;
use Exception;

class PemilikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $pemilik = Pemilik::paginate(10);
            
            return response()->json([
                    'success' => true,
                    'kode'=>200,
                    'message'=>'Berhasil',
                    'data' => $pemilik
                ]);
        } catch (Exception $error) {
            return response()->json([
                    'success' => false,
                    'kode'=>500,
                    'message'=>'gagal',
                ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'nama' =>'required',
                'email' =>'required',
                'alamat' =>'required'
            ]);

            $pemilik = Pemilik::create([
                'nama' =>$request->nama,
                'email' =>$request->email,
                'alamat' =>$request->alamat
            ]);
            
            $dataPemilik = Pemilik::where('id','=', $pemilik->id)->get();

            return response()->json([
                    'success' => true,
                    'kode'=>200,
                    'message'=>'Berhasil',
                    'data' => $dataPemilik
                ]);
        } catch (Exception $error) {
            return response()->json([
                    'success' => false,
                    'kode'=>500,
                    'message'=>'gagal',
                ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {           
            $dataPemilik = Pemilik::where('id','=', $id)->get();
            $dataBukuPemilik = Buku::where('pemilik_id','=',$id)->paginate(5);

            return response()->json([
                    'success' => true,
                    'kode'=>200,
                    'message'=>'Berhasil',
                    'pemilik' => $dataPemilik,
                    'buku' => $dataBukuPemilik
                ]);
        } catch (Exception $error) {
            return response()->json([
                    'success' => false,
                    'kode'=>500,
                    'message'=>'gagal',
                ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'nama' =>'required',
                'email' =>'required',
                'alamat' =>'required'
            ]);

            $pemilik = Pemilik::findOrFail($id);

            $pemilik->update([
                'nama' =>$request->nama,
                'email' =>$request->email,
                'alamat' =>$request->alamat
            ]);
            
            $dataPemilik = Pemilik::where('id','=', $id)->get();

            return response()->json([
                    'success' => true,
                    'kode'=>200,
                    'message'=>'Berhasil',
                    'data' => $dataPemilik
                ]);
        } catch (Exception $error) {
            return response()->json([
                    'success' => false,
                    'kode'=>500,
                    'message'=>'gagal',
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $pemilik = Pemilik::findOrFail($id);

            $pemilik->delete();

            return response()->json([
                    'success' => true,
                    'kode'=>200,
                    'message'=>'Berhasil'
                ]);
        } catch (Exception $error) {
            return response()->json([
                    'success' => false,
                    'kode'=>500,
                    'message'=>'gagal',
                ]);
        }
    }
}
