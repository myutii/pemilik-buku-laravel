<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Buku;
use Illuminate\Http\Request;
use Illuminate\Contracts\Support\Jsonable;
use Exception;
use Auth;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $buku = Buku::with('pemilik')->paginate(10);

            return response()->json([
                    'success' => true,
                    'kode'=>200,
                    'message'=>'Berhasil',
                    'data' => $buku
                ]);
        } catch (Exception $error) {
            return response()->json([
                    'success' => false,
                    'kode'=>500,
                    'message'=>'gagal',
                ]);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'judul' =>'required',
                'penulis' =>'required',
                'penerbit' =>'required',
                'pemilik_id' =>'required'
            ]);

            $buku = Buku::create([
                'judul' =>$request->judul,
                'penulis' =>$request->penulis,
                'penerbit' =>$request->penerbit,
                'pemilik_id' =>$request->pemilik_id
            ]);
            
            $dataBuku = Buku::where('id','=', $buku->id)->get();

            return response()->json([
                    'success' => true,
                    'kode'=>200,
                    'message'=>'Berhasil',
                    'data' => $dataBuku
                ]);
        } catch (Exception $error) {
            return response()->json([
                    'success' => false,
                    'kode'=>500,
                    'message'=>'gagal',
                ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
                    
            $dataBuku = Buku::where('id','=', $id)->with('pemilik')->get();

            return response()->json([
                    'success' => true,
                    'kode'=>200,
                    'message'=>'Berhasil',
                    'data' => $dataBuku,
                ]);
        } catch (Exception $error) {
            return response()->json([
                    'success' => false,
                    'kode'=>500,
                    'message'=>'gagal',
                ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'judul' =>'required',
                'penulis' =>'required',
                'penerbit' =>'required',
                'pemilik_id' =>'required'
            ]);

            $buku = Buku::findOrFail($id);

            $buku->update([
                'judul' =>$request->judul,
                'penulis' =>$request->penulis,
                'penerbit' =>$request->penerbit,
                'pemilik_id' =>$request->pemilik_id
            ]);
            
            $dataBuku = Buku::where('id','=', $id)->get();

            return response()->json([
                    'success' => true,
                    'kode'=>200,
                    'message'=>'Berhasil',
                    'data' => $dataBuku
                ]);
        } catch (Exception $error) {
            return response()->json([
                    'success' => false,
                    'kode'=>500,
                    'message'=>'gagal',
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $buku = Buku::findOrFail($id);
            
            $buku->delete();

            return response()->json([
                    'success' => true,
                    'kode'=>200,
                    'message'=>'Berhasil'
                ]);
        } catch (Exception $error) {
            return response()->json([
                    'success' => false,
                    'kode'=>500,
                    'message'=>'gagal',
                ]);
        }
    }
}
