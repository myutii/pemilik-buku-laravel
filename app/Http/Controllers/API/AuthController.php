<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Exception;

class AuthController extends Controller
{
    public function login(Request $request){
    	$credentials = $request->only('email','password');

    	$validator = Validator::make($credentials,[
    		'email' => 'required',
    		'password' => 'required',
    	]);

    	if($validator->fails()){
    		return response()->json($validator->errors()->toJson(),400);
    	}    	
    	
    	try {
    		if(!$token = JWTAuth::attempt($credentials)){
    			return response()->json(['error' => 'invalid_credentials'], 400);
    		}
    	} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
    		return response()->json(['error' => 'couldn_create_token'], 500);
    	}

    	return response()->json(compact('token'));
    }

    public function register(Request $request){
    	$validator = Validator::make($request->all(),[
    		'name' => 'required',
    		'email' => 'required',
    		'password' => 'required',
    	]);

    	if($validator->fails()){
    		return response()->json($validator->errors()->toJson(),400);
    	}
        try {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);

            $token = JWTAuth::fromUser($user);

            return response()->json(compact('user','token'), 201);
        } catch (Exception $e) {
            return response()->json(['email telah terdaftar'], 400);
        }
    	
    }

    public function getAuthenticatedUser(){
    	try {
    		if (!$user = JWTAuth::parseToken()->authenticate()) {
    			return response()->json(['user_not_found'],404);
    		}
    	} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
    		return response()->json(['token_expired'], $e->getStatusCode());
    	} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
    		return response()->json(['token_invalid'], $e->getStatusCode());
    	} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
    		return response()->json(['token_absent'], $e->getStatusCode());
    	}

    	return response()->json(compact('user'));
    }
}

