<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\BukuController;
use App\Http\Controllers\API\PemilikController;
use App\Http\Controllers\API\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', [AuthController::class, 'login']);
Route::post('register', [AuthController::class, 'register']);

Route::group(['prefix' => 'buku', 'middleware' => 'jwt.verify'], function () {
    Route::get('/', [BukuController::class, 'index']);
    Route::get('/{id}', [BukuController::class, 'show']);
    Route::post('tambah', [BukuController::class, 'store']);
    Route::post('ubah/{id}', [BukuController::class, 'update']);
    Route::delete('hapus/{id}', [BukuController::class, 'destroy']);
});

Route::group(['prefix' => 'pemilik', 'middleware' => 'jwt.verify'], function () {
    Route::get('/', [PemilikController::class, 'index']);
    Route::get('/{id}', [PemilikController::class, 'show']);
    Route::post('tambah', [PemilikController::class, 'store']);
    Route::post('ubah/{id}', [PemilikController::class, 'update']);
    Route::delete('hapus/{id}', [PemilikController::class, 'destroy']);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
