<?php

namespace Database\Seeders;

use App\Models\Pemilik;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PemilikSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'nama' => 'Ajeng',
                'email' => 'ajeng@mail.com',
                'alamat' => 'Jl. Kenangan',
            ],
            [
                'nama' => 'Anita',
                'email' => 'anita@mail.com',
                'alamat' => 'Jl. Kenangan',
            ],
            [
                'nama' => 'Ashila',
                'email' => 'ashila@mail.com',
                'alamat' => 'Jl. Kenangan',
            ],
            [
                'nama' => 'Aris',
                'email' => 'aris@mail.com',
                'alamat' => 'Jl. Kenangan',
            ],
            [
                'nama' => 'Bintang',
                'email' => 'bintang@mail.com',
                'alamat' => 'Jl. Kenangan',
            ],
            [
                'nama' => 'Bobi',
                'email' => 'bobi@mail.com',
                'alamat' => 'Jl. Kenangan',
            ],
            [
                'nama' => 'Budi',
                'email' => 'budi@mail.com',
                'alamat' => 'Jl. Kenangan',
            ],
            [
                'nama' => 'Bulan',
                'email' => 'bulan@mail.com',
                'alamat' => 'Jl. Kenangan',
            ],
            [
                'nama' => 'Cempaka',
                'email' => 'cempaka@mail.com',
                'alamat' => 'Jl. Kenangan',
            ],
            [
                'nama' => 'Delima',
                'email' => 'delima@mail.com',
                'alamat' => 'Jl. Kenangan',
            ],
            [
                'nama' => 'Dika',
                'email' => 'dika@mail.com',
                'alamat' => 'Jl. Kenangan',
            ],
            [
                'nama' => 'Kenanga',
                'email' => 'kenanga@mail.com',
                'alamat' => 'Jl. Kenangan',
            ],
            [
                'nama' => 'Juwita',
                'email' => 'juwita@mail.com',
                'alamat' => 'Jl. Kenangan',
            ],
            [
                'nama' => 'Malik',
                'email' => 'malik@mail.com',
                'alamat' => 'Jl. Kenangan',
            ],
            [
                'nama' => 'Sarah',
                'email' => 'sarah@mail.com',
                'alamat' => 'Jl. Kenangan',
            ],
        ];

        foreach ($data as $row) {
            Pemilik::create($row);
        }
    }
}
