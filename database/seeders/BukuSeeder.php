<?php

namespace Database\Seeders;

use App\Models\Buku;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class BukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
                'judul' => 'Tentang Kamu',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Rindu',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Pulang',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Pergi',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Janji',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Negeri Para Bedebah',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Negeri Di Ujung Tanduk',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Bedebah Di Ujung Tanduk',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Pulang-Pergi',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Bumi',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Bulan',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Matahari',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Bintang',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Komet',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Komet Minor',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Sagaras',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Hujan',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Rasa',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Sunset Bersama Rosie',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Harga Sebuah Percaya',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 1,
        	],
        	[
                'judul' => 'Tentang Kamu',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 2,
        	],
        	[
                'judul' => 'Tentang Kamu',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 3,
        	],
        	[
                'judul' => 'Tentang Kamu',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 4,
        	],
        	[
                'judul' => 'Tentang Kamu',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 5,
        	],
        	[
                'judul' => 'Tentang Kamu',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 6,
        	],
        	[
                'judul' => 'Tentang Kamu',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 7,
        	],
        	[
                'judul' => 'Tentang Kamu',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 8,
        	],
        	[
                'judul' => 'Tentang Kamu',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 9,
        	],
        	[
                'judul' => 'Tentang Kamu',
                'penulis' => 'Tere Liye',
                'penerbit'=> 'Republika',
                'pemilik_id' => 10,
        	],
        ];

        foreach ($data as $row) {
            Buku::create($row);
        }
    }
}
